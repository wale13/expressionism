/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./public/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./public/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./public/src/styles/styles.css":
/*!****************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./public/src/styles/styles.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "html,\nbody {\n    height: 100%;\n    width: 100%;\n    margin: 0;\n    padding: 0;\n}\n\n#root {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    width: 100%;\n}\n\n.fighters {\n    display: flex;\n    justify-content: space-between;\n    align-content: flex-start;\n    align-items: flex-start;\n    flex: 1;\n    flex-wrap: wrap;\n    padding: 15px;\n}\n\n.fighter {\n    display: flex;\n    flex-direction: column;\n    padding: 20px;\n}\n\n.fighter:hover {\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\n    cursor: pointer;\n}\n\n.name {\n    align-self: center;\n    font-size: 21px;\n    margin-top: 20px;\n}\n\n.fighter-image {\n    height: 260px;\n}\n\n#loading-overlay, #modal {\n    position: fixed;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    font-size: 18px;\n    background: rgba(255, 255, 255, 0.7);\n    visibility: hidden;\n}\n\n#players-panel {\n    margin-top: 25px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    padding: 15px;\n}\n\n#player1, #player2 {\n    height: 150px;\n    width: 150px;\n    background-color: rgb(200, 200, 200);\n    background-size: contain;\n    background-repeat: no-repeat;\n    margin: 0 15px;\n    box-shadow: -5px 10px 20px 0 rgba(0,0,0,0.2);\n}\n\n#player2, .x-scaled {\n    transform:scaleX(-1);\n}\n\n.vs {\n    font-size: xx-large;\n    text-shadow: 0 0 5px rgb(50, 50, 50);\n    color: white;\n}\n\n.app-controls {\n    display: flex;\n    justify-content: center;\n    padding-top: 10px;\n}\n\n.app-controls input {\n    border: 2px solid #bbbbbb;\n    background-color: white;\n    padding: 16px 32px;\n    text-align: center;\n    text-decoration: none;\n    display: inline-block;\n    font-size: 16px;\n    margin: 0 10px;\n    transition-duration: 0.4s;\n    cursor: pointer;\n    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);\n    border-radius: 5px;\n}\n\ninput.disabled {\n    opacity: 0.6;\n    cursor: not-allowed;\n}\n\nh1 {\n    text-align: center;\n    padding-top: 10px;\n}\n\n.modal-body {\n    display: flex;\n    background: wheat;\n    padding: 10px;\n    box-shadow: 0 20px 30px rgba(100, 100, 100, 0.5);\n}\n\n.modal-body .fighter-details {\n    display: flex;\n    flex-direction: column;\n    align-items: flex-end;\n    padding: 5px;\n}\n\n.modal-body .fighter-details span {\n    padding-bottom: 5px;\n}\n\n.modal-body .fighter-avatar {\n    height: 200px;\n}\n\n.modal-body .fighter-name {\n    align-self: center;\n    padding: 10px 0;\n    margin-bottom: 20px;\n    font-size: 21px;\n}\n\n.modal-body .fighter-health {\n    color: red;\n}\n\n.modal-body .fighter-attack {\n    color: blue;\n}\n\n.modal-body .fighter-defense {\n    color: green;\n}\n\n.fighter-details input[type='text'] {\n    text-align: center;\n    width: 40px;\n    padding: 5px 7px;\n}\n\n.modal-buttons {\n    align-self: stretch;\n    display: flex;\n    justify-content: space-around;\n    flex: 1 0 0;\n    align-items: flex-end;\n}\n\n.battleground {\n    display: flex;\n    width: 100%;\n    height: 100%;\n    justify-content: center;\n    align-items: center;\n}\n\n.left-fighter, .right-fighter {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n}\n\n.left-bar, .right-bar {\n    text-align: center;\n    width: 75%;\n    background: linear-gradient(to right, red 100%, white 100%);\n    font-size: small;\n    color: rgb(220, 220, 0);\n    border-radius: 8px;\n    border: 1px solid red;\n    margin-bottom: 15px;\n}\n\n.left-popup, .right-popup {\n    position: relative;\n    top: -200px;\n    visibility: hidden;\n    padding: 15px;\n    background-image: radial-gradient(yellow 30%, rgba(0, 0, 0, 0) 80%);\n}\n\n.message {\n    position: absolute;\n    left: calc(50% - 150px);\n    top: 45%;\n    padding: 50px;\n    background-color: rgba(255, 255, 255, 0.9);\n    border: 1px solid rgb(200, 200, 200);\n    border-radius: 10px;\n    box-shadow: -5px 10px 20px rgb(200, 200, 200);\n    visibility: hidden;\n    text-align: center;\n    z-index: 2;\n}\n", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./public/index.js":
/*!*************************!*\
  !*** ./public/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./public/src/javascript/app.js");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.css */ "./public/src/styles/styles.css");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__);


new _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["default"]();

/***/ }),

/***/ "./public/src/javascript/app.js":
/*!**************************************!*\
  !*** ./public/src/javascript/app.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fightersView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fightersView */ "./public/src/javascript/fightersView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/fightersService */ "./public/src/javascript/services/fightersService.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




class App {
  constructor() {
    this.startApp();
  }

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      const fighters = await _services_fightersService__WEBPACK_IMPORTED_MODULE_1__["fighterService"].getFighters();
      const fightersView = new _fightersView__WEBPACK_IMPORTED_MODULE_0__["default"](fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

}

_defineProperty(App, "rootElement", document.getElementById('root'));

_defineProperty(App, "loadingElement", document.getElementById('loading-overlay'));

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./public/src/javascript/battle.js":
/*!*****************************************!*\
  !*** ./public/src/javascript/battle.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./public/src/javascript/view.js");
/* harmony import */ var _fighter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighter */ "./public/src/javascript/fighter.js");



class Battle extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(players) {
    super();
    this.player1 = players.get('player1');
    this.player2 = players.get('player2');
    this.createBattle();
  }

  createBattle() {
    const fighter1 = new _fighter__WEBPACK_IMPORTED_MODULE_1__["default"](this.player1);
    const fighter2 = new _fighter__WEBPACK_IMPORTED_MODULE_1__["default"](this.player2, 'right');
    this.renderBattleGround();
    this.startBattle(fighter1, fighter2);
  }

  renderBattleGround() {
    const root = document.querySelector('body');
    const divs = document.querySelectorAll('div');
    const leftFighter = this.createFighter(this.player1, 'left');
    const rightFighter = this.createFighter(this.player2, 'right');
    const message = this.createElement({
      tagName: 'span',
      className: 'message'
    });
    [...divs].forEach(div => div.remove());
    this.element = this.createElement({
      tagName: 'div',
      className: 'battleground'
    });
    this.element.append(leftFighter, rightFighter);
    root.prepend(message, this.element);
  }

  createFighter(fighter, position) {
    let element = this.createElement({
      tagName: 'div',
      className: `${position}-fighter`
    });
    const healthBar = this.createElement({
      tagName: 'span',
      className: `${position}-bar`
    });
    const imageElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes: {
        src: fighter.source
      }
    });
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    const popupElement = this.createElement({
      tagName: 'div',
      className: `${position}-popup`
    });
    popupElement.innerText = '1';

    if (position === 'right') {
      imageElement.classList.add('x-scaled');
    }

    healthBar.innerText = fighter.health + ' hp';
    nameElement.innerText = fighter.name;
    element.append(nameElement, healthBar, imageElement, popupElement);
    return element;
  }

  startBattle(attacker, defender) {
    const showHit = (message, position) => {
      const elementName = `.${position}-popup`;
      const targetElement = document.querySelector(elementName);
      targetElement.innerText = message;
      targetElement.style.visibility = 'visible';
      setTimeout(() => targetElement.style.visibility = 'hidden', 500);
    };

    (function nextHit() {
      setTimeout(function () {
        const hitPower = attacker.getHitPower();
        const blockPower = defender.getBlockPower();
        const damage = hitPower - blockPower;
        const healthBar = document.querySelector(`.${defender.position}-bar`);
        let remainingHpPercentage;

        if (damage > 0) {
          defender.currentHealth - damage > 0 ? defender.currentHealth -= damage : defender.currentHealth = 0;
          remainingHpPercentage = Math.floor(defender.currentHealth / (defender.health / 100));
          healthBar.style.background = `linear-gradient(to right, red ${remainingHpPercentage}%, white ${remainingHpPercentage}%)`;
          healthBar.innerText = defender.currentHealth + ' hp';
          showHit(`-${damage} hp`, defender.position);
        } else {
          showHit('Block!', defender.position);
        }

        if (defender.currentHealth > 0) {
          [attacker, defender] = [defender, attacker];
          nextHit();
        } else {
          const messageElement = document.querySelector('.message');
          messageElement.innerHTML = `<h1><b>${attacker.name}</b> wins!!!</h1><p>Game will restart soon...</p>`;
          messageElement.style.visibility = 'visible';
          setTimeout(() => location.reload(), 7000);
        }
      }, 750);
    })();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Battle);

/***/ }),

/***/ "./public/src/javascript/fighter.js":
/*!******************************************!*\
  !*** ./public/src/javascript/fighter.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class Fighter {
  constructor(fighterDetails, position = 'left') {
    this.name = fighterDetails.name;
    this.health = Number(fighterDetails.health);
    this.attackPower = fighterDetails.attack;
    this.defensePower = fighterDetails.defense;
    this.source = fighterDetails.source;
    this.position = position;
    this.currentHealth = this.health;
  }

  getHitPower() {
    const max = 2;
    const min = 1;

    const criticalHitChance = (() => Math.floor(Math.random() * max + min))();

    const power = this.attackPower * criticalHitChance;
    return power;
  }

  getBlockPower() {
    const max = 2;
    const min = 1;

    const dodgeChance = (() => Math.floor(Math.random() * max + min))();

    const power = this.defensePower * dodgeChance;
    return power;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Fighter);

/***/ }),

/***/ "./public/src/javascript/fighterView.js":
/*!**********************************************!*\
  !*** ./public/src/javascript/fighterView.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./public/src/javascript/view.js");


class FighterView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighter, handleClick) {
    super();
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter));
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterView);

/***/ }),

/***/ "./public/src/javascript/fightersView.js":
/*!***********************************************!*\
  !*** ./public/src/javascript/fightersView.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./public/src/javascript/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighterView */ "./public/src/javascript/fighterView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/fightersService */ "./public/src/javascript/services/fightersService.js");
/* harmony import */ var _modalView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modalView */ "./public/src/javascript/modalView.js");
/* harmony import */ var _battle__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./battle */ "./public/src/javascript/battle.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class FightersView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters) {
    super();

    _defineProperty(this, "fightersDetailsMap", new Map());

    _defineProperty(this, "playersMap", new Map());

    const resetBtn = document.getElementById('reset');
    resetBtn.addEventListener('click', () => {
      location = '/reset';
    });
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_1__["default"](fighter, this.handleClick);
      return fighterView.element;
    });
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    try {
      const {
        _id: id
      } = fighter;
      const fightersMap = this.fightersDetailsMap;
      const fighterDetails = await _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].getFighterDetails(id);
      fightersMap.set(id, fighterDetails);
      const modalView = new _modalView__WEBPACK_IMPORTED_MODULE_3__["default"](event, id, fightersMap.get(id));
      const modalBody = modalView.element;
      const modal = document.getElementById('modal');
      modal.appendChild(modalBody);
      modal.style.visibility = 'visible';
      const selectButton = document.querySelector('.confirm-btn');
      const deleteButton = document.querySelector('.delete-btn');

      const selectFighters = () => {
        const fighterDetails = modalView.selectFighter();
        const player1 = document.getElementById('player1');
        const player2 = document.getElementById('player2');
        const playersMap = this.playersMap;

        if (!playersMap.has('player1')) {
          playersMap.set('player1', fighterDetails);
          player1.style.backgroundImage = `url("${fighterDetails.source}")`;
        } else {
          if (fighterDetails.name === playersMap.get('player1').name) {
            playersMap.set('player1', fighterDetails);
          } else {
            playersMap.set('player2', fighterDetails);
            player2.style.backgroundImage = `url("${fighterDetails.source}")`;
            const startBtn = document.getElementById('start');
            startBtn.classList.remove('disabled');
            startBtn.disabled = false;
            startBtn.addEventListener('click', () => this.startFight());
          }
        }

        _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].updateFighterDetails(fighterDetails);
        modalView.closeModal();
      };

      selectButton.addEventListener('click', selectFighters);
      deleteButton.addEventListener('click', () => {
        _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].deleteFighter(id);
        location = '/';
      });
    } catch (error) {
      console.warn(error);
    }
  }

  startFight() {
    new _battle__WEBPACK_IMPORTED_MODULE_4__["default"](this.playersMap);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightersView);

/***/ }),

/***/ "./public/src/javascript/helpers/apiHelper.js":
/*!****************************************************!*\
  !*** ./public/src/javascript/helpers/apiHelper.js ***!
  \****************************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
function callApi(endpoint, method, data) {
  const url = endpoint;
  let options = {
    method
  };

  if (data) {
    options = {
      headers: {
        'Content-type': 'application/json'
      },
      method,
      body: JSON.stringify(data)
    };
  }

  return fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).catch(error => {
    throw error;
  });
}



/***/ }),

/***/ "./public/src/javascript/modalView.js":
/*!********************************************!*\
  !*** ./public/src/javascript/modalView.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./public/src/javascript/view.js");
function _classPrivateFieldGet(receiver, privateMap) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } var descriptor = privateMap.get(receiver); if (descriptor.get) { return descriptor.get.call(receiver); } return descriptor.value; }

function _classPrivateFieldSet(receiver, privateMap, value) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to set private field on non-instance"); } var descriptor = privateMap.get(receiver); if (descriptor.set) { descriptor.set.call(receiver, value); } else { if (!descriptor.writable) { throw new TypeError("attempted to set read only private field"); } descriptor.value = value; } return value; }



class ModalView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(event, id, fighterDetails) {
    super();

    _id2.set(this, {
      writable: true,
      value: void 0
    });

    _name.set(this, {
      writable: true,
      value: void 0
    });

    _source.set(this, {
      writable: true,
      value: void 0
    });

    _classPrivateFieldSet(this, _id2, id);

    _classPrivateFieldSet(this, _name, fighterDetails.name);

    _classPrivateFieldSet(this, _source, fighterDetails.source);

    this.createModal(fighterDetails);
  }

  createModal(fighterDetails) {
    const {
      health,
      attack,
      defense
    } = fighterDetails;
    const nameElement = this.createName(_classPrivateFieldGet(this, _name));
    const healthElement = this.createHealth(health);
    const attackElement = this.createAttack(attack);
    const defenseElement = this.createDefense(defense);
    const avatar = this.createAvatar(_classPrivateFieldGet(this, _source));
    const buttons = this.createButtons();
    const elements = [nameElement, healthElement, attackElement, defenseElement, buttons];
    this.element = this.createElement({
      tagName: 'div',
      className: 'modal-body'
    });
    this.element.append(avatar);
    const detailsView = this.createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    detailsView.append(...elements);
    this.element.append(detailsView);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'fighter-name'
    });
    nameElement.innerHTML = `Name: <b>${name}</b>`;
    return nameElement;
  }

  createHealth(health) {
    const healthElement = this.createElement({
      tagName: 'span',
      className: 'fighter-health'
    });
    const attributes = {
      type: 'text',
      value: health
    };
    const healthInput = this.createElement({
      tagName: 'input',
      className: 'input-health',
      attributes
    });
    healthElement.append('Health: ', healthInput);
    return healthElement;
  }

  createAttack(attack) {
    const attackElement = this.createElement({
      tagName: 'span',
      className: 'fighter-attack'
    });
    const attributes = {
      type: 'text',
      value: attack
    };
    const attackInput = this.createElement({
      tagName: 'input',
      className: 'input-attack',
      attributes
    });
    attackElement.append('Attack: ', attackInput);
    return attackElement;
  }

  createDefense(defense) {
    const defenseElement = this.createElement({
      tagName: 'span',
      className: 'fighter-defense'
    });
    const attributes = {
      type: 'text',
      value: defense
    };
    const defenseInput = this.createElement({
      tagName: 'input',
      className: 'input-defense',
      attributes
    });
    defenseElement.append('Defense: ', defenseInput);
    return defenseElement;
  }

  createAvatar(source) {
    const attributes = {
      src: source
    };
    const avatarElement = this.createElement({
      tagName: 'img',
      className: 'fighter-avatar',
      attributes
    });
    return avatarElement;
  }

  createButtons() {
    const buttons = this.createElement({
      tagName: 'div',
      className: 'modal-buttons'
    });
    const linkToView = this;
    const confirmButton = createConfirmBtn();
    const cancelButton = createCancelBtn();
    const deleteButton = createDeleteBtn();

    function createConfirmBtn() {
      const attributes = {
        type: 'button',
        value: 'Select'
      };
      const confirmButton = linkToView.createElement({
        tagName: 'input',
        className: 'confirm-btn',
        attributes
      });
      return confirmButton;
    }

    function createCancelBtn() {
      const attributes = {
        type: 'button',
        value: 'Cancel'
      };
      const cancelButton = linkToView.createElement({
        tagName: 'input',
        className: 'cancel-btn',
        attributes
      });
      return cancelButton;
    }

    function createDeleteBtn() {
      const attributes = {
        type: 'button',
        value: 'Delete'
      };
      const deleteButton = linkToView.createElement({
        tagName: 'input',
        className: 'delete-btn',
        attributes
      });
      return deleteButton;
    }

    cancelButton.addEventListener('click', linkToView.closeModal);
    buttons.append(confirmButton, cancelButton, deleteButton);
    return buttons;
  }

  selectFighter() {
    const _id = _classPrivateFieldGet(this, _id2);

    const name = _classPrivateFieldGet(this, _name);

    const source = _classPrivateFieldGet(this, _source);

    const health = Number(document.querySelector('.input-health').value);
    const attack = Number(document.querySelector('.input-attack').value);
    const defense = Number(document.querySelector('.input-defense').value);
    const finalDetails = {
      _id,
      name,
      source,
      health,
      attack,
      defense
    };
    return finalDetails;
  }

  closeModal() {
    const modal = document.getElementById('modal');
    modal.innerHTML = '';
    modal.style.visibility = 'hidden';
  }

}

var _id2 = new WeakMap();

var _name = new WeakMap();

var _source = new WeakMap();

/* harmony default export */ __webpack_exports__["default"] = (ModalView);

/***/ }),

/***/ "./public/src/javascript/services/fightersService.js":
/*!***********************************************************!*\
  !*** ./public/src/javascript/services/fightersService.js ***!
  \***********************************************************/
/*! exports provided: fighterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighterService", function() { return fighterService; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./public/src/javascript/helpers/apiHelper.js");


class FighterService {
  static async getContent(endPoint, method = 'GET', data) {
    try {
      const endpoint = endPoint;
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, method, data);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  getFighters() {
    const endpoint = '/user';
    return FighterService.getContent(endpoint);
  }

  getFighterDetails(_id) {
    const endpoint = `/user/${_id}`;
    return FighterService.getContent(endpoint);
  }

  updateFighterDetails(fighterDetails) {
    const id = fighterDetails._id;
    const endpoint = `/user/${id}`;
    const method = 'PUT';
    const data = fighterDetails;
    FighterService.getContent(endpoint, method, data);
  }

  deleteFighter(id) {
    const endpoint = `/user/${id}`;
    const method = 'DELETE';
    FighterService.getContent(endpoint, method);
  }

}

const fighterService = new FighterService();

/***/ }),

/***/ "./public/src/javascript/view.js":
/*!***************************************!*\
  !*** ./public/src/javascript/view.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class View {
  constructor() {
    _defineProperty(this, "element", void 0);
  }

  createElement({
    tagName,
    className = '',
    attributes = {}
  }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    return element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./public/src/styles/styles.css":
/*!**************************************!*\
  !*** ./public/src/styles/styles.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./public/src/styles/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map