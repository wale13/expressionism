function callApi(endpoint, method, data) {
  const url = endpoint;
  let options = {
    method
  };
  
  if (data) {
    options = { 
      headers: {
        'Content-type': 'application/json'
      },
      method,
      body: JSON.stringify(data)
    };
  }

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }