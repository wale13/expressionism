import View from './view';
import Fighter from './fighter';

class Battle extends View {
  constructor(players) {
    super();

    this.player1 = players.get('player1');
    this.player2 = players.get('player2');

    this.createBattle();
  }

  createBattle() {
    const fighter1 = new Fighter(this.player1);
    const fighter2 = new Fighter(this.player2, 'right');

    this.renderBattleGround();
    this.startBattle(fighter1, fighter2);
  }

  renderBattleGround() {
    const root = document.querySelector('body');
    const divs = document.querySelectorAll('div');
    const leftFighter = this.createFighter(this.player1, 'left');
    const rightFighter = this.createFighter(this.player2, 'right');
    const message = this.createElement({ tagName: 'span', className: 'message' });

    [...divs].forEach((div) => div.remove());
    this.element = this.createElement({ tagName: 'div', className: 'battleground' });
    this.element.append(leftFighter, rightFighter);
    root.prepend(message, this.element);
  }

  createFighter(fighter, position) {
    let element = this.createElement({ tagName: 'div', className: `${position}-fighter` });
    const healthBar = this.createElement({
      tagName: 'span',
      className: `${position}-bar`
    });
    const imageElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes: { src: fighter.source }
    });
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    const popupElement = this.createElement({ tagName: 'div', className: `${position}-popup` });
    popupElement.innerText = '1';

    if (position === 'right') {
      imageElement.classList.add('x-scaled');
    }

    healthBar.innerText = fighter.health + ' hp';
    nameElement.innerText = fighter.name;
    element.append(nameElement, healthBar, imageElement, popupElement);
    return element;
  }

  startBattle(attacker, defender) {
    const showHit = (message, position) => {
      const elementName = `.${position}-popup`;
      const targetElement = document.querySelector(elementName);
  
      targetElement.innerText = message;
      targetElement.style.visibility = 'visible';

      setTimeout(() => targetElement.style.visibility = 'hidden', 500);
    };

    (function nextHit() {
      setTimeout(function() {
        const hitPower = attacker.getHitPower();
        const blockPower = defender.getBlockPower();
        const damage = hitPower - blockPower;
        const healthBar = document.querySelector(`.${defender.position}-bar`);
        let remainingHpPercentage;

        if (damage > 0) {
          (defender.currentHealth - damage) > 0
          ? defender.currentHealth -= damage
          : defender.currentHealth = 0;

          remainingHpPercentage = Math.floor(defender.currentHealth / (defender.health / 100));
          healthBar.style.background = `linear-gradient(to right, red ${remainingHpPercentage}%, white ${remainingHpPercentage}%)`;
          healthBar.innerText = defender.currentHealth + ' hp';

          showHit(`-${damage} hp`, defender.position);
        } else {
          showHit('Block!', defender.position);
        }

        if (defender.currentHealth > 0) {
          [attacker, defender] = [defender, attacker];
          nextHit();
        } else {
          const messageElement = document.querySelector('.message');

          messageElement.innerHTML = `<h1><b>${attacker.name}</b> wins!!!</h1><p>Game will restart soon...</p>`;
          messageElement.style.visibility = 'visible';
          setTimeout(() => location.reload(), 7000);
        }
      }, 750)
    })();
  }
}

export default Battle;
