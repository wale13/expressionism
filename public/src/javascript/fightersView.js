import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import ModalView from './modalView';
import Battle from './battle';

class FightersView extends View {
  constructor(fighters) {
    super();

    const resetBtn = document.getElementById('reset');

    resetBtn.addEventListener('click', () => {
      location = '/reset';
    });
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  playersMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    try {
      const { _id: id } = fighter;
      const fightersMap = this.fightersDetailsMap;
      const fighterDetails = await fighterService.getFighterDetails(id);

      fightersMap.set(id, fighterDetails);
      
      const modalView = new ModalView(event, id, fightersMap.get(id));
      const modalBody = modalView.element;
      const modal = document.getElementById('modal');

      modal.appendChild(modalBody);
      modal.style.visibility = 'visible';
      
      const selectButton = document.querySelector('.confirm-btn');
      const deleteButton = document.querySelector('.delete-btn');
      const selectFighters = () => {
        const fighterDetails = modalView.selectFighter();
        const player1 = document.getElementById('player1');
        const player2 = document.getElementById('player2');
        const playersMap = this.playersMap;
        
        if (!playersMap.has('player1')) {
          playersMap.set('player1', fighterDetails);
          player1.style.backgroundImage = `url("${fighterDetails.source}")`;
        } else {
          if (fighterDetails.name === playersMap.get('player1').name) {
            playersMap.set('player1', fighterDetails);
          } else {
            playersMap.set('player2', fighterDetails);
            player2.style.backgroundImage = `url("${fighterDetails.source}")`;

            const startBtn = document.getElementById('start');

            startBtn.classList.remove('disabled');
            startBtn.disabled = false;
            startBtn.addEventListener('click', () => this.startFight());
          }
        }

        fighterService.updateFighterDetails(fighterDetails);

        modalView.closeModal();
      }

      selectButton.addEventListener('click', selectFighters);
      deleteButton.addEventListener('click', () => {
        fighterService.deleteFighter(id);
        location = '/';
      });
    } catch (error) {
        console.warn(error);
    }
  }
  
  startFight() {
    new Battle(this.playersMap);
  }
}

export default FightersView;
