import { callApi } from '../helpers/apiHelper';

class FighterService {
  static async getContent(endPoint, method = 'GET', data) {
    try {
      const endpoint = endPoint;
      const apiResult = await callApi(endpoint, method, data);

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  getFighters() {
    const endpoint = '/user';
    
    return FighterService.getContent(endpoint);
  }

  getFighterDetails(_id) {
    const endpoint = `/user/${_id}`;

    return FighterService.getContent(endpoint);
  }

  updateFighterDetails(fighterDetails) {
    const id = fighterDetails._id;
    const endpoint = `/user/${id}`;
    const method = 'PUT';
    const data = fighterDetails;

    FighterService.getContent(endpoint, method, data);
  }

  deleteFighter(id) {
    const endpoint = `/user/${id}`;
    const method = 'DELETE';

    FighterService.getContent(endpoint, method);
  }
}

export const fighterService = new FighterService();
