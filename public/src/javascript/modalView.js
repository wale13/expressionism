import View from './view';

class ModalView extends View {
  #id;
  #name;
  #source;

  constructor(event, id, fighterDetails) {
    super();
    
    this.#id = id;
    this.#name = fighterDetails.name;
    this.#source = fighterDetails.source;
    this.createModal(fighterDetails);
  }

  createModal(fighterDetails) {
    const { health, attack, defense } = fighterDetails;
    const nameElement = this.createName(this.#name);
    const healthElement = this.createHealth(health);
    const attackElement = this.createAttack(attack);
    const defenseElement = this.createDefense(defense);
    const avatar = this.createAvatar(this.#source);
    const buttons = this.createButtons();
    const elements = [
      nameElement,
      healthElement,
      attackElement,
      defenseElement,
      buttons
    ];
    
    this.element = this.createElement({ tagName: 'div', className: 'modal-body' });
    this.element.append(avatar);

    const detailsView = this.createElement({ tagName: 'div', className: 'fighter-details' });

    detailsView.append(...elements);
    this.element.append(detailsView);

  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'fighter-name' });
    nameElement.innerHTML = `Name: <b>${name}</b>`;

    return nameElement;
  }

  createHealth(health) {
    const healthElement = this.createElement({ tagName: 'span', className: 'fighter-health' });
    const attributes = { type: 'text', value: health };
    const healthInput = this.createElement({ tagName: 'input', className: 'input-health', attributes });

    healthElement.append('Health: ', healthInput);

    return healthElement;
  }

  createAttack(attack) {
    const attackElement = this.createElement({ tagName: 'span', className: 'fighter-attack' });
    const attributes = { type: 'text', value: attack };
    const attackInput = this.createElement({ tagName: 'input', className: 'input-attack', attributes });

    attackElement.append('Attack: ', attackInput);

    return attackElement;
  }

  createDefense(defense) {
    const defenseElement = this.createElement({ tagName: 'span', className: 'fighter-defense' });
    const attributes = { type: 'text', value: defense };
    const defenseInput = this.createElement({ tagName: 'input', className: 'input-defense', attributes });

    defenseElement.append('Defense: ', defenseInput);

    return defenseElement;
  }

  createAvatar(source) {
    const attributes = { src: source };
    const avatarElement = this.createElement({
      tagName: 'img',
      className: 'fighter-avatar',
      attributes
    });

    return avatarElement;
  }

  createButtons() {
    const buttons = this.createElement({ tagName: 'div', className: 'modal-buttons' });
    const linkToView = this;
    const confirmButton = createConfirmBtn();
    const cancelButton = createCancelBtn();
    const deleteButton = createDeleteBtn();

    function createConfirmBtn() {
      const attributes = { type: 'button', value: 'Select' };
      const confirmButton = linkToView.createElement({
        tagName: 'input',
        className: 'confirm-btn',
        attributes
      });

      return confirmButton;
    }

    function createCancelBtn() {
      const attributes = { type: 'button', value: 'Cancel' };
      const cancelButton = linkToView.createElement({
        tagName: 'input',
        className: 'cancel-btn',
        attributes
      });

      return cancelButton;
    }

    function createDeleteBtn() {
      const attributes = { type: 'button', value: 'Delete' };
      const deleteButton = linkToView.createElement({
        tagName: 'input',
        className: 'delete-btn',
        attributes
      });

      return deleteButton;
    }

    cancelButton.addEventListener('click', linkToView.closeModal);

    buttons.append(confirmButton, cancelButton, deleteButton);
    return buttons;
  }

  selectFighter() {
    const _id = this.#id;
    const name = this.#name;
    const source = this.#source;
    const health = Number(document.querySelector('.input-health').value);
    const attack = Number(document.querySelector('.input-attack').value);
    const defense = Number(document.querySelector('.input-defense').value);
    const finalDetails = { _id, name, source, health, attack, defense };

    return finalDetails;
  }

  closeModal() {
    const modal = document.getElementById('modal');

    modal.innerHTML = '';
    modal.style.visibility = 'hidden';
  }
}

export default ModalView;
