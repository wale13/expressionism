class Fighter {
  constructor(fighterDetails, position = 'left') {
    this.name = fighterDetails.name;
    this.health = Number(fighterDetails.health);
    this.attackPower = fighterDetails.attack;
    this.defensePower = fighterDetails.defense;
    this.source = fighterDetails.source;
    this.position = position;
    this.currentHealth = this.health;
  }

  getHitPower() {
    const max = 2;
    const min = 1;
    const criticalHitChance = (() => Math.floor(Math.random() * max + min))();
    const power = this.attackPower * criticalHitChance;

    return power;
  }

  getBlockPower() {
    const max = 2;
    const min = 1;
    const dodgeChance = (() => Math.floor(Math.random() * max + min))();
    const power = this.defensePower * dodgeChance;

    return power;
  }
}

export default Fighter;
