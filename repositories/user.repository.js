const fs = require('fs');

const readUserList = () => {
  const fileContent = fs.readFileSync('./repositories/userlist.json');
  const list = JSON.parse(fileContent);

  return list;
};

const writeUserList = data => {
  const newContent = JSON.stringify(data, null, 2);

  fs.writeFileSync('./repositories/userlist.json', newContent);
}

const getList = () => {
  const users = readUserList();

  return users;
};

const getUser = id => {
  const users = readUserList();
  const userData = users.find(user => user._id === id);

  return userData;
}

const addUser = userData => {
  let content = readUserList();

  content.push(userData);
  writeUserList(content);
};

const editUser = (id, userdata) => {
  const oldContent = readUserList();
  const newContent = oldContent.map(user => {
    if (user._id === id) {
      return userdata;
    } else {
      return user;
    }
  });

  writeUserList(newContent);
};

const deleteUser = id => {
  const oldContent = readUserList();
  const newContent = oldContent.filter(user => user._id !== id);

  writeUserList(newContent);
};

const reset = () => {
  const originalContent = fs.readFileSync('./repositories/userlist.original.json');

  writeUserList(JSON.parse(originalContent));
};

module.exports = {
  getList,
  getUser,
  addUser,
  editUser,
  deleteUser,
  reset
};