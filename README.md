# Street Fighter feat. Express.js - [View on Heroku](https://expressionism.herokuapp.com/ "Street Fighter")
***

## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:3000/

***
### Notes

- Every route established.
- All routers are used in app except `POST` router (_to add new `user`_). It wasn't implemented due to lack of time.
- App isn't perfect, but I'm proud of it :).
