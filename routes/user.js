const express = require('express');
const router = express.Router();

const { getUsers } = require('../services/get.users.service');
const { updateUsers } = require('../services/update.users.service');
const { validateData } = require('../middlewares/validate.middleware');

router.route('/')
  .get((req, res, next) => {
    const result = getUsers();

    result ? res.send(JSON.stringify(result))
    : res.status(400).send(JSON.stringify('Some error'));
  })
  .post(validateData, (req, res, next) => {
    const newUser = req.body;

    updateUsers(null, newUser);
    res.status(201).send(JSON.stringify('User created'));
  });

router.route('/:id')
  .get((req, res, next) => {
    const id = req.params.id;
    const result = getUsers(id);

    result ? res.send(JSON.stringify(result))
    : res.status(400).send(JSON.stringify(`Can't find that user`));
  })
  .put(validateData, (req, res, next) => {
    const id = req.params.id;
    const userData = req.body;

    updateUsers(id, userData);
    res.status(200).send(JSON.stringify('User updated'));
  })
  .delete((req, res, next) => {
    const id = req.params.id;

    updateUsers(id, null, 'delete');
    res.redirect('/');
  });

module.exports = router;
