const express = require('express');
const router = express.Router();

const { resetUsers } = require('../services/reset.users.service');

router.get('/', function(req, res, next) {
  resetUsers();
  res.redirect('/');
});

module.exports = router;
