const { getList, getUser } = require('../repositories/user.repository');

const getUsers = id => {
  if (!id) {
    return getList();
  } else {
    return getUser(id);
  }
};

module.exports = {
  getUsers
};
