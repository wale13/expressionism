const { reset } = require('../repositories/user.repository');

const resetUsers = () => {
  reset();
};

module.exports = {
  resetUsers
};
