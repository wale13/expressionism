const { addUser, editUser, deleteUser } = require('../repositories/user.repository');

const updateUsers = (id, data, wdFlag = 'write') => {
  if (!id) {
    addUser(data);
  } else if (wdFlag === 'write') {
    editUser(id, data);
  } else if (wdFlag === 'delete') {
    deleteUser(id);
  }
};

module.exports = {
  updateUsers
}