const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');
const resetRouter = require('./routes/reset');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(require('body-parser').urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/reset', resetRouter);

module.exports = app;
