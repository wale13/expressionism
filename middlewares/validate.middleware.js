const validateData = (req, res, next) => {
  if (
    req &&
    typeof req.body === 'object' &&
    req.body._id &&
    req.body.name &&
    req.body.health &&
    req.body.attack &&
    req.body.defense &&
    req.body.source
  ) {
    next();
  } else {
    res.status(409).send(JSON.stringify('Invalid data'));
  }
};

module.exports = {
  validateData
};
